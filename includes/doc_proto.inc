<?php

define(DOCS_PROTO_TABLE, 'document_prototypes');

class docPrototype {
  private $id, $name, $tokens, $markup;
  
  function __construct($_name, $_tokens = array(), $_markup = '') {
    
    if ( is_array($_tokens) && ($_markup !== '') && !empty($_name) ) {
      
      $this->name = $_name;
      $this->tokens = $_tokens;
      $this->markup = $_markup;
      
    }
    
  }
  
  /**
   * Operations:
   *  1. load - loads an "old" prototype from DB.
   *  2. save - saves a new prototype to DB.
   *  3. delete - deletes an existing prototype (fututre implementation, if the need will arise).
   *  4. maybe something else?
   */
  public function load($id) {
    //...
  }
  
  public function save() {
    
    if (! empty($this->id) ) {
      $msg = db_query("INSERT INTO {%s} SET `id`=%d,   `name`='%s', `tokens`='%s',            `markup`='%s'", 
                      DOCS_PROTO_TABLE,     $this->id, $this->name, serialize($this->tokens), $this->markup
                    );
    }
    else {
      $msg = db_query("UPDATE {%s} SET  `name`='%s', `tokens`='%s',            `markup`='%s' WHERE `id`=%d", 
                      DOCS_PROTO_TABLE, $this->name, serialize($this->tokens), $this->markup,      $this->id
                    );
    }
    
    if ($msg !== TRUE) {
      drupal_set_message(t('Document Prototyping: Unable to save prototype to database. Please make sure the module is installed correctly.'), 'error');
      return FALSE;
    }
    else {
      return $this;
    }
    
  }
  
// Getter method for "id" property:
  public function getId() {
    return $this->id;
  }

  
/**
 * Getter & Setter methods for "name" property.
 */
  public function getName() {
    return $this->name;
  }
  
  private function _set_name($_name = '') {
    
    $this->name = $_name;
    
  }
  
/**
 * Getter & Setter methods for "tokens" property.
 */
  public function getTokens() {
    return $this->tokens;
  }
  
  private function _set_tokens($_tokens = array()) {
    
  // NOTE: calling: $obj->set_tokens() will remove the tokens. 
  //       This is the expected behaviour. This goes to all other setter-methods around.
    if ($_tokens !== '') {
      $this->tokens = $_tokens;
    }
    
  }  
  
/**
 * Getter & Setter methods for "markup" property.
 */
  public function getMarkup() {
    return $this->markup;
  }
  
  private function _set_markup($_markup = '') {
    
    // Do we need to verify that it is valid HTML? WYSIWYGS might return invalid syntax so maybe we shouldn't check validy.
    $this->markup = $_markup;
    
  }
  
  
/**
 * Class methods
 */
  
  private function _replace_tokens($replacements) {
    // .... Implement replacement function probably using preg_match and preg_replace.
  }
  
  
/**
 * $obj->render($replacements);
 *  * $replacements - a token replacements hash.
 *  * @return the path to the output file.
 */
  public function render($replacements) {
  // Implement using 'print' module.
  }
  
  
// A "dump" method:
  public function __dump() {
    return array(
      'id'      => $this->getId(),
      'name'    => $this->getName(),
      'tokens'  => $this->getTokens(),
      'markup'  => $this->getMarkup(),
      );
  }
  
}
