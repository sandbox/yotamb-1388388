<?php

/**
 * This file will contain all the hooks and UIs
 *  necessary for creating and managing Prototypes.
 * After a prototype is created, it can be linked to a node.
 * 
 * Yotam: We need to figure out the best way to create a new prototype.
 *        - Will it be a new content type? (kinda messy)
 *        - Will it be just some pseudo dbo that loads each time a node of content type "prototype" is loaded?
 */


// Implementation of hook_menu.
function docs_proto_menu() {
  return array(
  
    'admin/settings/docs_proto' => array(
        'title'             => 'Document Prototyping',
        'page callback'     => 'docs_proto_settings_page',
        'access arguments'  => array('manage document prototypes'),
      ),
    'admin/settings/docs_proto/new' => array(
        'title'             => 'New Document Prototype',
        'page callback'     => 'docs_proto_new_proto',
        'access arguments'  => array('manage document prototypes'),
      ),
    'admin/settings/docs_proto/edit/%' => array(
        'title'             => 'Edit Document Prototype',
        'page callback'     => 'docs_proto_edit_proto',
        'access arguments'  => array('manage document prototypes'),
      ),
    
    );
}

function docs_proto_settings_page() {
  return 'kk';
}

function docs_proto_new_proto() {
  return drupal_get_form('docs_proto_new_proto_form');
}

/**
 * This is the "New Prototype" form. This is where "Prototypes" are born.
 * Right below this subroutine, there's the "submit" callback. Take a look at it. ;)
 */
function docs_proto_new_proto_form(&$form_state) {
  $form = array();
  
  //dpm($form_state, 'Form State');
  
  // General fields set
  $form['doc_proto_fieldset'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('New Document Prototype'),
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
    );
    
    // `Name` field
    $form['doc_proto_fieldset']['doc_proto_name'] = array(
      '#type'           => 'textfield',
      '#size'           => 20,
      '#title'          => t('Name'),
      '#description'    => t('The name for your new document prototype.'),
      '#weight'         => 0,
      '#default_value'  => !empty($form_state['values']['doc_proto_name']) ? $form_state['values']['doc_proto_name'] : '',
      );
    
    /*
    // Fieldset for the "Name" & "Markup/Template" fields
    $form['doc_proto_fieldset'] = array(
      '#type'         => 'fieldset',
      '#title'        => t('Prototype\'s Name & Template'),
      '#collapsible'  => TRUE,
      '#collapsed'    => FALSE,
      );
    */  
        
      /** `Markup` field
       *  TODO:
       *    1. [DONE] Make it a WYSIWYG field
       *    2. Make it larger. 100% width. I guess 600px height. It needs to contain full documents.
       */
       
      $form['doc_proto_fieldset']['doc_proto_markup_fieldset'] = array(
      '#type'         => 'fieldset',
      '#title'        => t('Template'),
      '#collapsible'  => TRUE,
      '#collapsed'    => FALSE,
      );
      
        $form['doc_proto_fieldset']['doc_proto_markup_fieldset']['doc_proto_markup'] = array(
          '#type'           => 'textarea',
          '#description'    => t('The template for your new document prototype.'),
          '#weight'         => 1,
          '#default_value'  => !empty($form_state['values']['doc_proto_markup']) ? $form_state['values']['doc_proto_markup'] : '',
          );
          
      $form['doc_proto_fieldset']['doc_proto_markup_fieldset']['format'] = filter_form(2);
      $form['doc_proto_fieldset']['doc_proto_markup_fieldset']['format']['#weight'] = 2;
      
    // Fieldset for all token-replacement pairs    
    $form['doc_proto_fieldset']['doc_proto_replacements_fieldset'] = array(
      '#type'         => 'fieldset',
      '#title'        => t('Tokens'),
      '#collapsible'  => TRUE,
      '#collapsed'    => FALSE,
      '#description'  => t('Add tokens that could be replaced when rendering a new document using this prototype.'),
      );
      
      // `Add Replacement` button
      $form['doc_proto_fieldset']['doc_proto_replacements_fieldset']['add_replacement'] = array(
        '#type'         => 'submit',
        '#value'        => t('Add Token'),
        '#validate'     => array('doc_proto_add_replacement'),
        );
    
    // Submit - save new Prototype  
    $form['doc_proto_fieldset']['sumbit'] = array(
      '#type'     => 'submit',
      '#value'    => t('Submit'),
      '#validate' => array('docs_proto_new_proto_form_validate'),
      );
      
    // Cancel - return to settigns page
    $form['doc_proto_fieldset']['reset'] = array(
      '#type'   => 'submit',
      '#value'  => t('Cancel'),
      );
  
  // Create the desired number of tokens
  for ($i = 0; $i < $form_state['storage']['token_count']; $i++) {
    // i-th token field
    $form['doc_proto_fieldset']['doc_proto_replacements_fieldset']['doc_proto_token_' . $i]['doc_proto_token_' . $i . '_token'] = array(
      '#type'           => 'textfield',
      '#size'           => 20,
      '#title'          => t('Token') . ' #' . $i,
      '#weight'         => 0,
      '#default_value'  => !empty($form_state['values']['doc_proto_token_' . $i . '_token']) ? $form_state['values']['doc_proto_token_' . $i . '_token'] : '',
      );
  }

  
  return $form;
}

/**
 *  Implementation of Submit Function for "New Prototype" form.
 *  Note: How does drupal knows this is the submit function for our form?
 *        The function names have a common string: "docs_proto_new_proto_form".
 *        Here, there's the additional "_submit". That's how drupal works. It's dirty
 *        but pretty simple. This goes to all the drupal "hooks".
 */
function docs_proto_new_proto_form_submit(&$form, &$form_state) {
  
  // Set the replacement (tokens) array: 
  dpm($form_state);
  $tokens = array();
  foreach ($form_state['values'] as $key => $val) {
    
    // If the key is guilty of being a token... hehehe
    if ( preg_match('/^doc_proto_token_/', $key) ) {
      $tokens[] = $val;
    }
    
  }
  
  // Create new prototype:
  $proto = new docPrototype($form_state['values']['doc_proto_name'], $tokens, $form_state['values']['doc_proto_markup']);
  //dpm($proto->__dump(), 'The Prototype');
  $proto->save();
  
}

// Validation for the settings page. Why not work you must?
function docs_proto_new_proto_form_validate(&$form, &$form_state) {
  
  $val = $form_state['values'];
  
  if (! $val['doc_proto_name']) {
    form_set_error('doc_proto_name', t('Name field cannot be empty.'));
  }
  
  if (! $val['doc_proto_markup']) {
    form_set_error('doc_proto_markup', t('Template field cannot be empty.'));
  }
  
}

/**
 * The "Add Token" callback function.
 * It raises the count of desired tokens, so in the "rebuild" the forn will be built with the right amount.
 */
function doc_proto_add_replacement(&$form, &$form_state) {
  if ( is_numeric($form_state['storage']['token_count']) ) {
    $form_state['storage']['token_count']++;
  }
  else {
    $form_state['storage']['token_count'] = 1;
  }

  // Setting $form_state['rebuild'] = TRUE causes the default submit
  // function to be skipped and the form to be rebuilt.
  $form_state['rebuild'] = TRUE;
}

/**
 * 
 *  eeeeeeeeeeee    dddddddddddddd      iiiiiiiiiiii    tttttttttttttttt
 *  eeeeeeeeeeee    ddd         ddd     iiiiiiiiiiii    tttttttttttttttt
 *  eee             ddd          ddd        iiii              tttt
 *  eee             ddd           ddd       iiii              tttt
 *  eeeeeeeeeeee    ddd           ddd       iiii              tttt
 *  eeeeeeeeeeee    ddd           ddd       iiii              tttt
 *  eee             ddd           ddd       iiii              tttt
 *  eee             ddd          ddd        iiii              tttt
 *  eeeeeeeeeeee    ddd         ddd     iiiiiiiiiiii          tttt
 *  eeeeeeeeeeee    dddddddddddddd      iiiiiiiiiiii          tttt
 *  
 * 
 *  The "Edit Prototype" form:
 */

function docs_proto_edit_proto() {
  return 'Um.. Wha?';
}
